<?php

class Majeur extends Personne{
    private $vote;
    
    
    public function __construct($nom,$prenom,$age,bool $vote)
    {
        
        parent::__construct($nom,$prenom,$age);
        $this->vote = $vote;
        
    }

    /**
     * Get the value of vote
     */ 
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set the value of vote
     *
     * @return  self
     */ 
    public function setVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }
    public function afficher()
    {
        $message ='nom : '.$this->getNom().' prénom : ' .$this->getPrenom(). ' âge: '.$this->getAge();
        $message.= " vote : " .($this->getVote()==true)?'vote':'ne vote pas';
        echo $message;
    }
}