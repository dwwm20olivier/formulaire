<?php

class Enfant extends Mineur{

    private $jeux;
    
    public function __construct($nom,$prenom,$age,$classe,String $jeux)
    {
        parent::__construct($nom,$prenom,$age,$classe);
        $this->jeux = $jeux;
    }

    /**
     * Get the value of jeux
     */ 
    public function getJeux():string
    {
        return $this->jeux;
    }

    /**
     * Set the value of jeux
     *
     * @return  self
     */ 
    public function setJeux($jeux)
    {
        $this->jeux = $jeux;

        return $this;
    }

    public function afficher()
    {
        $message = parent::afficher();
        $message .= $this->getJeux();
        echo $message;
    }
}