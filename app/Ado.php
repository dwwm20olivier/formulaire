<?php

class Ado extends Mineur
{

    private $portable;

    public function __construct($nom, $prenom, $age, $classe, bool $portable)
    {
        parent::__construct($nom, $prenom, $age, $classe);
        $this->portable = $portable;
    }

    /**
     * Get the value of portable
     */
    public function getPortable(): bool
    {
        return $this->portable;
    }

    /**
     * Set the value of portable
     *
     * @return  self
     */
    public function setPortable($portable)
    {
        $this->portable = $portable;

        return $this;
    }

    public function afficher()
    {

        $message = parent::afficher();
        $message .= ($this->getPortable() == true) ? " oui" : ' non"';
        echo $message;
    }
}
