<?php

class Mineur extends Personne{

    private $classe;

    public function __construct($nom,$prenom,$age, String $classe)
    {
        parent::__construct($nom,$prenom,$age);
        $this->classe = $classe;
    }



    /**
     * Get the value of classe
     */ 
    public function getClasse():string
    {
        return $this->classe;
    }

    /**
     * Set the value of classe
     *
     * @return  self
     */ 
    public function setClasse($classe)
    {
        $this->classe = $classe;

        return $this;
    }

    public function afficher()
    {
        $message ='nom : '.$this->getNom().' prénom : ' .$this->getPrenom(). ' âge: '.$this->getAge();
        $message .= 'classe' .$this->getClasse();
        echo $message;
    }
}