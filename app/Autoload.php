<?php

class Autoload{

    static function register()
    {
        spl_autoload_register([__CLASS__,'autol']);
    }
    static function autol($class)
    {
        require 'app/'.$class.'.php';
    }
}