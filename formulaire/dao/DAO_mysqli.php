<?php

class DAO_mysqli implements Dao{

    private $mysqli;

    public function __construct()
    {
        $this->connexion();
    }

    public function requete($sql)
    {
        
        if(!$result = $this->mysqli->query($sql)){
            echo "désolé la connexion n'a pu être établie";
            echo "Error: Notre requête a échoué lors de l'exécution et voici pourquoi :\n";
            echo "Query: " . $sql . "\n";
            echo "Errno: " . $this->mysqli->errno . "\n";
            echo "Error: " . $this->mysqli->error . "\n";
            exit;
        }
        return true;
    }

    public function connexion()
    {
        $this->mysqli = new mysqli('localhost', 'root', '', 'exoformulaire',3308);
        if ($this->mysqli->connect_errno) {
            // La connexion a échoué. Que voulez-vous faire ? 
            // Vous pourriez vous contacter (email ?), enregistrer l'erreur, afficher une jolie page, etc.
            // Vous ne voulez pas révéler des informations sensibles

            // Essayons ceci :
            echo "Désolé, le site web subit des problèmes.";

            // Quelque chose que vous ne devriez pas faire sur un site public,
            // mais cette exemple vous montrera quand même comment afficher des
            // informations lié à l'erreur MySQL -- vous voulez peut être enregistrer ceci
            echo "Error: Échec d'établir une connexion MySQL, voici pourquoi : \n";
            echo "Errno: " . $this->mysqli->connect_errno . "\n";
            echo "Error: " . $this->mysqli->connect_error . "\n";

            // Vous voulez peut être leurs afficher quelque chose de jolie, nous ferons simplement un exit
            exit;
        }
    }

    public function query()
    {
        
        $result = $this->mysqli->query('SELECT * FROM user');
        return  mysqli_fetch_all($result,MYSQLI_ASSOC);
    }
}