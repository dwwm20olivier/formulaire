<?php

class Dao_pgsql{

    private $pdo;

    public function __construct()
    {
        $this->connexion();
    }

    public function connexion()
    {
        try{
            $this->pdo = new PDO('pgsql:host=localhost;dbname=demo','postgres','root');
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function requete($sql)
    {
        try{

            var_dump($this->pdo->query($sql));
        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }

    public function query()
    {
        $result = $this->pdo->query('SELECT * FROM student');
        return $result;
    }
}