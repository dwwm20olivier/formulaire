<?php

class Dao_Pdo implements Dao{


    private $pdo;
    public function __construct()
    {
        $this->connexion();
    }
    public function requete($sql)
    {
        $this->pdo->query($sql);
    }

    private function connexion()
    {
        $config = Config::$connection;
        try{
            $this->pdo = new PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'].';port=3308',$config['user'],$config['password']);
        }catch(PDOException $e){
            print $e->getMessage();
        }
    }

    public function query($sql)
    {
        return $this->pdo->query($sql);
    }

    
    public function delete($sql)
    {
        $this->pdo->query($sql);
    }

   
}