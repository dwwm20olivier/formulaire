<?php

require 'Autoload.php';
Autoload::register();
$c = $_GET['c'];
$q = $_GET['q'];
$ctrl = new $c;
if (isset($c)) {
    if (method_exists($ctrl, $q)) {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $ctrl->$q($id);
        } else if (isset($_GET['stat'])) {
            $obj = $_GET['stat'];
            $ctrl->$q($obj);
        } else {
            $ctrl->$q();
        }
    } else {
        header('Location:http://localhost/afpaphp/formulaire/CtrlContact/Error');
    }
} else {
    header('Location:http://localhost/afpaphp/formulaire/CtrlContact/Error');
}
