<?php

class Autoload{

    static function register()
    {
        spl_autoload_register([__CLASS__,'autol']);
    }
    static function autol($class)
    {
        $fol = ['Controller','model','view','dao','config'];
        foreach($fol as $k=>$v){
            if(file_exists($v.'/'.$class.'.php')){
                require $v.'/'.$class.'.php';
            }else{
                
            }
            
        }
    }
}