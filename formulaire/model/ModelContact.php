<?php

class ModelContact{

    private $dao;
    public function __construct()
    {
        $this->dao = new Dao_pdo;
    }
     
    public function inserer($data)
    {
        
        $requete = "INSERT INTO user 
        VALUES(
                null,
                '{$data['nom']}',
                '{$data['prenom']}',
                '{$data['message']}',
                '{$data['tel']}',
                '{$data['email']}',
                '{$data['civilite']}',
                '{$data['objet']}',
                0
            )";
        
        $this->dao->requete($requete);
        return true;
    }

    public function afficher()
    {
        $sql = 'SELECT * FROM user as u 
        LEFT JOIN objet as o ON u.idobjet = o.idobjet
        LEFT JOIN civilite as c ON c.idcivilite = u.idcivilite
        WHERE supprimer=0';
        return $this->dao->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    
    public function objet()
    {
        $requete = 'SELECT * FROM objet';
        return $this->dao->query($requete)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function select($id)
    {
        $requete = "SELECT * FROM user as u 
        LEFT JOIN objet as o ON u.idobjet = o.idobjet
        LEFT JOIN civilite as c ON c.idcivilite = u.idcivilite
        WHERE id = $id";
        return $this->dao->query($requete)->fetch(PDO::FETCH_ASSOC);
    }

    public function modifier($data,$id)
    {
        
        $sql = "UPDATE user SET 
        nom='{$data['nom']}', prenom='{$data['prenom']}', 
        message='{$data['message']}', tel='{$data['tel']}', email='{$data['email']}',  
        idcivilite='{$data['civilite']}',  idobjet='{$data['objet']}', supprimer=0 
        WHERE id=$id ";
        $this->dao->requete($sql);

        
    }

    public function supprimer($id)
    {
        $sql = "UPDATE user SET supprimer=1 WHERE id=$id";
        $this->dao->delete($sql);
    }

    public function stat()
    {
        $tab = [];
        $sql ="SELECT COUNT(id) as homme FROM user WHERE idcivilite=1";
        $sql1 ="SELECT COUNT(id) as femme FROM user WHERE idcivilite=2";
        $homme = $this->dao->query($sql)->fetch(PDO::FETCH_ASSOC);
        $femme = $this->dao->query($sql1)->fetch(PDO::FETCH_ASSOC);
        $tab = array_merge($femme,$homme);
        return $tab;
    }

    public function findByObjet($obj)
    {
        $sql = "SELECT * FROM user as u 
        LEFT JOIN objet as o ON u.idobjet = o.idobjet
        LEFT JOIN civilite as c ON c.idcivilite = u.idcivilite
        WHERE u.idobjet = $obj";

            return $this->dao->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        

    }
    
}