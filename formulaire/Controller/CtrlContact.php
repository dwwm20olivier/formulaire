<?php
class CtrlContact
{

    private $view;
    private $model;
    public function __construct()
    {
        $this->model = new ModelContact();
        $this->view = new ViewContact();
    }

    public function getForm()
    {
        $data = $this->model->objet();
        $this->view->afficherForm($data);
    }

    public function update($id)
    {
        $data = $this->model->select($id);
        $objet = $this->model->objet();

        $this->view->afficherEdit($data,$objet);
    }

    private function verifier($name)
    {
        return true;
    }

    public function enregForm()
    {
        if (isset($_POST['submit'])) {
            $_SESSION['succes'] = "votre enregistrement a bien été créé";
            $data = $_POST;
            if ($this->verifier($data)) {
                $_SESSION['success'] = "article enregistré";
                $this->model->inserer($data);
                $this->view->afficherFormOk($data);
            } else {
                $this->view->afficherFormNotOk();
            }
        }
    }

    public function liste()
    {
        $data = $this->model->afficher();
        $this->view->show($data);
    }

    public function modifier($id)
    {
        $_SESSION['success'] = "votre article a bien été modifié";
        if (isset($_POST['submit'])) {

            $data = $_POST;
            if ($this->verifier($data)) {

                $this->model->modifier($data,$id);
                header('Location:http://localhost/afpaphp/formulaire/CtrlContact/liste');
            } else {
                $this->view->afficherFormNotOk();
            }
        }
    }

    public function delete($id)
    {
        $this->model->supprimer($id);
        $data = $this->model->afficher();
        $this->view->show($data);
    }

    public function statistique(?string $obj=null)
    {
        $objet = $this->model->objet();
        $data = $this->model->stat();
        if(!empty($obj)){
            $result = $this->model->findByObjet($obj);
        }else{$result = null;}
        $this->view->stat($data,$objet,$result);
    }

    

    public function export()
    {
        $data = $this->model->afficher();
        $this->view->showCsv($data);
    }

    public function error()
    {
        $this->view->error();
    }

}
