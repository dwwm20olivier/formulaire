<?php

class ViewContact{

    private $dao;
    public function __construct()
    {
        $this->dao = new Dao_pdo;
    }

    public function afficherForm($objet)
    {
        $contenu = "form";
        include 'view/layout.php';
    }
    
    public function afficherFormOk($data)
    {
        $contenu = "ok";
        include "view/layout.php";
        
    }

    public function afficherFormNotOk()
    {
        $contenu = "notok";
        include "view/layout.php";

    }

    public function show($result)
    {
        $contenu = "list";
        include "view/layout.php";

    }
    public function afficherEdit($data,$objet)
    {
        $contenu = "update";
        include "view/layout.php";

    }
    public function stat($data,$objet,$result)
    {
        $contenu = "stat";
        include "view/layout.php";

    }

    public function showCsv($data)
    {
        $contenu = "contactcsv";
        include "view/csv.php";

    }

    public function error(){
        $contenu ="error";
        include "view/layout.php";

    }

    
}