<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>formulaire</title>
    <base href="http://localhost/afpaphp/formulaire/">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
    <?php include "template/nav.php" ?>
    <div class="container" style="margin-top:150px;">
        <?php include 'template/' . $contenu . ".php" ?>
    </div>
</body>

</html>